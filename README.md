# Run Xilinx Tools on Docker

## Abstract
This repository provides everything you need to run Xilinx tools on Docker container.

## Prerequisites
This dockerfile is tested under WSL2 + Docker Desktop.  
You need Windows 11 environment as WSLg is required.  
For more information about WSL2, visit Microsoft's documentation.  

## How to build
You have 3 steps to build docker container.

### (1): build `ubuntu-x11` docker image
At first, you need to build X11 related Ubuntu image.
Type following commands.

```
$ cd ubuntu-x11
$ docker build --progress plain -t ubuntu-x11 .
```

After `docker build` succeeds, try following command to run the image.
```
 $ docker run --rm -it --net host -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY ubuntu-x11
```

Try running some X11 apps (e.g. `xeyes`) and test if it works.


### (2): Download and prepare Xilinx installer
 1. Download [Xilinx Unified Installer SFD(Single File Download)](https://www.xilinx.com/member/forms/download/xef.html?filename=Xilinx_Unified_2022.2_1014_8888.tar.gz), not "Web Installer". 
 2. Open WSL terminal and `cd` into your download directory.
 3. Run `$ python3 -m http.server`
 4. Keep python3 process running.

### (3): build `ubuntu-xilinx` docker image
Run following command to build the image.
It takes several hours, be patient.
```
 $ cd ubuntu-xilinx
 $ docker build --progress plain -t ubuntu-xilinx .
```

Done? Great!

## How to use
 1. Open WSL terminal and `cd` into your working directory.
 2. Run Xilinx container by running following command.
    ```
     $ docker run --rm -it --net host -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY -v $PWD:/work ubuntu-xilinx
    ```
 3. Load and apply Xilinx setting file.
    ```
     $ source /tools/Xilinx/Vitis/2022.2/settings64.sh
    ```
 4. And it's your turn!

## Know issues
### vivado get abnormally termineted with `realloc(): invalid old size` error
Vivado sometimes gets terminated with weird `realloc()` error.
This problem is reported and discussed in [Xilinx Forum](https://support.xilinx.com/s/question/0D54U00005Sgst2SAB/failed-batch-mode-execution-in-linux-docker-running-under-windows-host?language=ja).
Try setting `LD_PRELOAD=/lib/x86_64-linux-gnu/libudev.so.1` before launching vivado.
```
 $ LD_PRELOAD=/lib/x86_64-linux-gnu/libudev.so.1 vivado
```