#!/bin/bash

export USER=xilinx
uid=$(stat -c "%u" .)
gid=$(stat -c "%g" .)

if [ "$uid" -ne 0 ]; then
  usermod -u $uid $USER
fi

if [ "$gid" -ne 0 ]; then
  groupmod -g $gid xilinx
fi

export HOME=/work
export SHELL=/bin/bash
export LOGNAME=xilinx
exec setpriv --reuid=$USER --regid=$USER --init-groups "$@"
